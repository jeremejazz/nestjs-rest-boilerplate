<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

# NestJS REST API Boilerplate

## Description

NestJS REST API Boilerplate

## Features

- Database ([typeorm](https://typeorm.io))
- Seeding (workaround. see /src/database/seeds)
- Config Service ([@nestjs/config](https://www.npmjs.com/package/@nestjs/config))

## Development

1. Copy and rename `.env.example` to `.env`

1. Configure `.env` file based on current host and DB settings

```
APP_DESCRIPTION=
APP_VERSION=
API_PREFIX=v1
PORT=3000
CORS_ACCESS_CONTROL_ALLOW_ORIGIN=*

POSTGRES_HOST=
POSTGRES_PORT=
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_DATABASE=
```

3. Install packages

```
npm install
```

4. Start server

```
npm typeorm:migration:run

npm run start:dev
```
