export enum UserType {
  Admin = 'admin',
  Partner = 'partner',
  Customer = 'customer',
}
