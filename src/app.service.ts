import { HttpStatus, Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class AppService {
  async getHealth() {
    return {
      statusCode: HttpStatus.OK,
      message: 'Success',
    };
  }
}
