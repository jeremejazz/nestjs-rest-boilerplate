import { UserType } from '../../const/userType';
import { UserEntity } from '../../models/user.entity';
import { AppDataSource } from '../data-source';
import * as bcrypt from 'bcrypt';
import { randomUUID } from 'crypto';
import chalk from 'chalk';
/** Example seeder script
 *
 * npx ts-node src/database/seeds/example-seeder.ts
 */

AppDataSource.initialize()
  .then(async () => {
    console.log('Inserting a new user into the database...');
    const user = new UserEntity();
    user.first_name = 'Timber';
    user.last_name = 'Saw';
    user.email = 'example2';
    user.user_type = UserType.Partner;
    user.password = bcrypt.hashSync('1234', 1);
    user.role_id = randomUUID();

    await AppDataSource.manager.save(user);
    console.log('Saved a new user with id: ' + user.user_id);
  })
  .catch((error) => console.log(error));
