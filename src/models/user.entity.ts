import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { adminStatus } from '../const/adminStatus.enum';

@Entity('users')
@Unique(['email'])
@Unique(['generated_id'])
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  user_id: string;

  @Column()
  @Index('userEmailIdx')
  email: string;

  @Column({
    nullable: true,
  })
  first_name: string;
  @Column({
    nullable: true,
  })
  middle_name: string;
  @Column({
    nullable: true,
  })
  last_name: string;

  @Column({
    type: 'varchar',
    nullable: true,
    select: false,
  })
  password: string;

  @Column()
  role_id: string;

  @Column()
  user_type: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  activation_code: string;

  @Column({
    nullable: true,
  })
  redirect_url: string;

  @Column({
    type: 'enum',
    enum: adminStatus,
    default: adminStatus.PENDING,
  })
  @Index('adminUsrStatusIdx')
  status: adminStatus;

  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  deleted_at: Date;

  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  activation_date: Date;

  @Column({
    nullable: true,
  })
  generated_id: string;

  /**
   * Use temporary as basis for inactive status ( date + 24h < current timestamp). Create new field for activation code generate date when
   * resend activation is going to be implemeneted
   */
  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  date_created: Date;

  @Column({
    nullable: true,
  })
  @Index('billingStoreIdUserIdx')
  billing_store_id: string;
}
