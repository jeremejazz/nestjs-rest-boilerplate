import { Controller, Get } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Get('list')
  public async getUsers() {
    const data = await this.userService.getUsers();
    return data;
  }
}
