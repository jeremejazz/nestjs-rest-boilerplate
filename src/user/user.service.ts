import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { UserEntity } from '../models/user.entity';

@Injectable()
export class UserService {
  constructor(private dataSource: DataSource) {}

  async getUsers() {
    const data = await this.dataSource
      .getRepository(UserEntity)
      .createQueryBuilder('u')
      .select('first_name')
      .getRawMany();
    return data;
  }
}
