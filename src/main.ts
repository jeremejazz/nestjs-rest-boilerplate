import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  app.setGlobalPrefix(configService.get('API_PREFIX'));

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  // enable cors
  const allowOrigin = process.env.CORS_ACCESS_CONTROL_ALLOW_ORIGIN || '*';
  Logger.log(`Access Control Allow Origin set to : ${allowOrigin}`);
  app.enableCors({
    origin: allowOrigin,
  });

  // API Documentation
  const APP_NAME = configService.get('APP_NAME');
  const APP_VERSION = configService.get('APP_VERSION') || '1.0';

  const options = new DocumentBuilder()
    .addBearerAuth()
    .setTitle(APP_NAME)
    .setDescription(process.env.APP_DESCRIPTION)
    .setVersion(APP_VERSION)
    .addBearerAuth({ in: 'header', type: 'http' })
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(process.env.PORT || 3000);
  Logger.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
